export class Article {
  id: string = "-1"
  title: string = "Defaulttitle"
  picture: string = ""
  body: string = "Defaultbody"
  creationDate: Date = new Date()
  lastModification: Date = new Date()

}
