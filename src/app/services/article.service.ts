import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Article } from '../models/article';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})

export class ArticleService {

  //Injection du service httpClient  
  constructor(private http:HttpClient) { }

  getById(id: string) {
    this.http.get("localhost:44358/api/article", { responseType: 'blob' }).subscribe(
      (data: any) => {
        
          const reader = new FileReader();
          reader.onloadend = (e) => {
            const readerResult = reader.result;
            console.log(readerResult); // Print blob content as text
          };
          reader.readAsText(data, 'UTF-8'); },
      (error: any) => { console.error(error) }
      )
    return new Article();
   
  }
}

